/// <reference path="./toast.d.ts" />
import { nanoid } from "nanoid";
import { defineStore } from "pinia";

export const useToastStore = defineStore('toastStore', {
  state() {
    return {
      messages: [] as YMessage[]
    }
  },
  actions: {
    pushMessage(data: YMessage) {
      data.id = nanoid();
      this.messages.unshift(data);
    },
    removeMessage(id: string) {
      const index = this.messages.findIndex((item) => item.id == id);
      if (index >= 0) {
        this.messages.splice(index, 1);
      }
    }
  }
})