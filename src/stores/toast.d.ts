type YMessageType = 'success' | 'error' | 'alert'

interface YMessage {
  id?: string,
  type: YMessageType,
  title?: string,
  text: string,
  time?: number,
  icon?: string
}