declare interface Song {
    id?: string,
    name: string,
    singer: string,
    album: string,
    url: string,
    lrc?: string,
}
declare type SongList = Array<Song>

declare type playerStateEnum = 'stop' | 'playing' | 'pause'
declare interface PlayerState {
    state: playerStateEnum,
    index?: number,
}