/// <reference path="./player.d.ts" />

import { defineStore } from 'pinia'
import { MusicList } from '../consts/list'
import { nanoid } from 'nanoid'

export const usePlayerStore = defineStore('playerStore', {
    state: () => ({
        // 播放状态
        playerState: 'stop' as playerStateEnum,
        // 播放位置
        index: 0,
        // 播放列表
        list: [] as SongList,
        // 历史播放
        history: [] as SongList,
        playingSource: {} as Song
    }),
    getters: {
        playing: (state) => state.playerState,
        currentIndex: (state) => state.index,
        currentlist: (state) => state.list.map(v => {
            v.id = nanoid(); // 创建 id
            return v;
        }),
        playSource: (state) => {
            if (state.list.length) {
                return state.list[state.index];
            } else {
                return state.playingSource;
            }
        }
    },
    actions: {
        getList() {
            this.list = MusicList
        },
        setPlayerState(state: playerStateEnum) {
            this.playerState = state;
        },
        playNext() {
            const next = this.index + 1;
            this.index = next >= this.list.length ? 0 : next; // 如果下一首是队尾，返回队首
        },
        playPrev() {
            const last = this.index - 1;
            this.index = last < 0 ? this.list.length - 1 : last; // 如果下一首是队首，返回队尾
        },
        playById(id?: string) {
            if (id) {
                this.index = this.list.findIndex((item) => item.id == id);
            }
        }
    }
})