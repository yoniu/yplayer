export const calcTime = (time: number) => {
  let minute = Math.floor(time / 60);
  minute = (minute.toString().length === 1 ? ('0' + minute) : minute) as number
  let second = Math.round(time % 60)
  second = (second.toString().length === 1 ? ('0' + second) : second) as number
  return `${minute}:${second}`
}
