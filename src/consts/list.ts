export const MusicList: SongList = [
    {
        name: '我想到你就再也不怕',
        singer: '徐佳莹',
        album: 'http://p2.music.126.net/LkwTIyU7Zo853W6LUfYpag==/109951167574776183.jpg',
        url: 'https://link.jscdn.cn/1drv/aHR0cHM6Ly8xZHJ2Lm1zL3UvcyFBaG9Bb3NIbnVIQkJsendsek82bmlKM0ZhMy1BP2U9ajE3STNU.m4a'
    },
    {
        name: '还在流浪',
        singer: '周杰伦',
        album: 'https://p2.music.126.net/VldbGI7kjph0TeIbttQHGQ==/109951167672625652.jpg',
        url: 'https://link.jscdn.cn/1drv/aHR0cHM6Ly8xZHJ2Lm1zL3UvcyFBaG9Bb3NIbnVIQkJsaUZJYzJHRDZaUy1VRTVGP2U9UkdYcDUw.flac',
    },
    {
        name: 'Finest Hour',
        singer: 'Gavin DeGraw',
        album: 'https://p1.music.126.net/kHPD4DlJD5ZmbOv42CQFYg==/109951165967342186.jpg',
        url: 'https://link.jscdn.cn/1drv/aHR0cHM6Ly8xZHJ2Lm1zL3UvcyFBaG9Bb3NIbnVIQkJna3hZVk9iMDVEU1FtOThpP2U9NjM2bEx6.mp3'
    },
    {
        name: '行走的鱼',
        singer: '徐佳莹',
        album: 'http://p2.music.126.net/UJxfKH09h5uQCHHHOhqI_g==/109951167890949700.jpg',
        url: 'https://link.jscdn.cn/1drv/aHR0cHM6Ly8xZHJ2Lm1zL3UvcyFBaG9Bb3NIbnVIQkJsem44ZVpQQVlxTzRSWkIxP2U9UjBtQ21m.m4a'
    },
    {
        name: '幸福',
        singer: '齐豫',
        album: 'http://p2.music.126.net/zUmKUF8i5YNgCNndhNRfUA==/109951166004150278.jpg',
        url: 'https://link.jscdn.cn/1drv/aHR0cHM6Ly8xZHJ2Lm1zL3UvcyFBaG9Bb3NIbnVIQkJsejJNdDhmN3lxVzRGa0l4P2U9OW1nNGRz.m4a'
    },
];